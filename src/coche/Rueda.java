package coche;

/**
 * La clase rueda contiene un bote que se llena con céntimos para pagar un depósito. 
 * @author Sdomnic

 */
public class Rueda {

    /**
      *  Combustible que queda en centilitros
     */
    private int combustible_queda_cl;  
    /**
      * Centimos que cuesta un litro
     */
    private int centimos_litro;
    /**
      * Capacidad del depósito

     */
    private static int CAPACIDAD = 55;//capacidad depósito
    /**
      * Litros que consume por cada 100km
 
     */
    private static int CONSUMO_MEDIO = 6;//litros a los cien kilometros
    /**
     * Centimos del bote

     */
    private int centimos_bote;

    /**
     * Constructor por defecto
     */
    public Rueda() {
    }

    /**
     * Constructor por parámetros <br>
 Incluye el combustible que queda en centilitros, los centimos por litro, los centimos del bote, y la CAPACIDAD del depósito.
     * @param combustible_queda_cl Combustible que queda en centilitros
     * @param centimos_litro Centimos que cuesta un litro
     * @param bote Centimos a añadir al bote
     * @param capacidad Capacidad del depósito
     */
    public Rueda(int combustible_queda_cl, int centimos_litro, int bote, int capacidad) {
        this.combustible_queda_cl = combustible_queda_cl;
        this.centimos_litro = centimos_litro;
        this.CAPACIDAD = capacidad;
        this.centimos_bote = bote;
    }

    /**
     * El método viaje comprueba si tenemos combustible suficiente para recorrer los kilómetros que pasemos por parámetro.
     * Al finalizar, elimina el combustible del depósito que se ha gastado.
     * En caso de que no haya suficiente combustible, lanzará una excepción.
     * En caso de que los kilómetros sean negativos, lanzará una excepción.
     * 
     * @param km Número de kilometros a realizar. 
     * @throws Exception 
     */
    
    public void viaje(int km) throws Exception {
        if (consumo(km) > (this.combustible_queda_cl / 100)) {
            throw new Exception("No hay suficiente combustible para esa distancia");
        }
        if (km <= 0) {
            throw new Exception("No tiene sentido recorrer una distancia que no sea positiva");
        }
        this.combustible_queda_cl = this.combustible_queda_cl - (consumo(km));
        System.out.println(combustible_queda_cl);
    }

    /**
     * Método que permite aumentar el combustible_queda_cl del depósito, es
     * necesario que la cantidad que se quiere añadir y el dinero con que se va
     * a pagar sean positivos, además el dinero junto con lo que había de bote
     * debe ser suficiente para pagar la cantidad que se va a añadir.
     * @param centilitros Centilitros a añadir al depósito.
     * @param centimos Céntimos a añadir al bote.
     * @return Devuelve los céntimos sobrantes.
     * @throws Exception 
     */
    public int rellenar(int centilitros, int centimos) throws Exception {
        if (centimos + this.centimos_bote <= 0) {
            throw new Exception("Se necesita una cantidad positiva de dinero");
        }
        if (centilitros <= 0) {
            throw new Exception("Los centilitros deben ser positivos");
        }

        if ((this.combustible_queda_cl + centilitros) / 100  > CAPACIDAD) {
            throw new Exception("No se puede superar la capacidad del depósito");
        }
        if (centimos + centimos_bote < (centilitros / 100) * centimos_litro) {
            throw new Exception("No tiene dinero suficiente");
        }
        this.combustible_queda_cl = this.combustible_queda_cl + centilitros;
        return (centimos + centimos_bote) - (centilitros / 100) * centimos_litro;
    }
    
    /**
     * Calcula el consumo del coche en centilitros en un viaje.
     * @param km Kilometros a recorrer
     * @return Consumo en centilitros
     */
    private int consumo(int km) {
        int consumo = km * CONSUMO_MEDIO;

        return consumo;
    } 

}
